package com.prm392.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView navigateInstagram;
    private TextView btnLogin;
    private EditText username;
    private EditText password;

    void bindingView(){
        navigateInstagram.findViewById(R.id.sign_up);
        btnLogin.findViewById(R.id.btn_login_fb);
        username.findViewById(R.id.username);
        password.findViewById(R.id.password);
    }

    private void navigateActivity(View view) {
        Intent instagramIntent = new Intent(this, InstagramActivity.class);
        startActivity(instagramIntent);
    }

    void bindingAction(){
        navigateInstagram.setOnClickListener(this::navigateActivity);
        btnLogin.setOnClickListener(this::handleLogin);
    }

    private void handleLogin(View view) {
        String username = this.username.getText().toString();
        String password = this.password.getText().toString();
        if (username == "admin" && password == "123456"){
            Toast.makeText(MainActivity.this, "Login successfully!", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(MainActivity.this, "Login failed!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        bindingView();
        bindingAction();
    }

}