package com.prm392.myapplication;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

public class InstagramActivity extends AppCompatActivity {
    LinearLayout switchFacebook;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        switchFacebook = findViewById(R.id.switch_facebook);
//        switchFacebook.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                InstagramActivity.super.onBackPressed();
//            }
//        });

        getSupportActionBar().hide();
        setContentView(R.layout.activity_instagram);
    }

    private void switchFacebook() {
        Intent facebookIntent = new Intent(this, MainActivity.class);
        startActivity(facebookIntent);
    }
}
